package com.jastudio.arranumber;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

	private WebView w;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//set listeners for the buttons
		Button b = (Button) findViewById(R.id.play_button);
		b.setOnClickListener(this);
		b = (Button) findViewById(R.id.gem_button);
		b.setOnClickListener(this);
		b = (Button) findViewById(R.id.perk_button);
		b.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.play_button

					:
				Button b = (Button) findViewById(R.id.play_button);
				b.setBackgroundResource(R.drawable.play_button);
				startActivity(new Intent(MainActivity.this, GameActivity.class));
				break;
			case R.id.gem_button:
				break;

			case R.id.perk_button:
				break;
		}
	}
}
