package com.jastudio.arranumber;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {
	private static final int EASY_MODE = R.array.easy;
	private static final int NORMAL_MODE = R.array.normal;
	private static final int HARD_MODE = R.array.hard;

	private static final int DEFAULT_LEVEL = 1;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_select);

		//set up button clicks
		Button b = (Button) findViewById(R.id.level_easy);
		b.setOnClickListener(this);
		b = (Button) findViewById(R.id.level_normal);
		b.setOnClickListener(this);
		b = (Button) findViewById(R.id.level_hard);
		b.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.level_easy:
				startGame(EASY_MODE);
				break;
			case R.id.level_normal:
				startGame(NORMAL_MODE);
				break;
			case R.id.level_hard:
				startGame(HARD_MODE);
				break;
		}
	}

	private void startGame(int level) {
		setContentView(R.layout.gameplay);

		final String[] questions = getResources().getStringArray(level);

		TableLayout answers = (TableLayout) findViewById(R.id.answer_grid);
		TableRow r = new TableRow(this);

		int width, height;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
			width = getWindowManager().getDefaultDisplay().getWidth();
			height = getWindowManager().getDefaultDisplay().getHeight();
		} else {
			Point size = new Point();
			getWindowManager().getDefaultDisplay().getSize(size);
			width = size.x;
			height = size.y;
		}

		//TODO: better way to get width and height for webview
		BitmapFactory.Options dimens = new BitmapFactory.Options();
		dimens.inJustDecodeBounds = true;
		Bitmap bg = BitmapFactory.decodeResource(getResources(), R.mipmap.button_1x1, dimens);
		final int WV_PER_ROW = width / dimens.outWidth;

		for (int i = 0; i < questions.length; i++) {
			final WebView w = new WebView(this);

			//TODO: check length of math string to determine background resource


			TableRow.LayoutParams wParams = new TableRow.LayoutParams(dimens.outWidth, dimens.outHeight);
			wParams.setMargins(5, 0, 5, 5);
			w.setLayoutParams(wParams);

			w.setBackgroundColor(Color.TRANSPARENT);
			w.setBackgroundResource(R.mipmap.button_1x1);
			w.getSettings().setJavaScriptEnabled(true);

			w.setOnTouchListener(null);
			String mimeType = "text/html";
			String encoding = "utf-8";
			String jaxpath = "file:///android_asset/MathJax/MathJax.js";
			String html = "<head><script type='text/x-mathjax-config'>"
					+ "MathJax.Hub.Config({ "
					+ "showMathMenu: false,"
					+ "jax: ['input/TeX', 'output/HTML-CSS'], "
					+ "extensions: ['tex2jax.js'], "
					+ "TeX: { extensions: ['AMSmath.js', 'AMSsymbols.js' ] }, "
					+ "messageStyle: 'none'"
					+ "});"
					+ "</script>"
					+ "<script type='text/javascript' src=" + jaxpath + "></script>"
					+ "<style>"
					+ "body { color: white; display: flex; justify-content: center; align-items: center; font-size: 150%;}"
					+ "#math { padding-bottom: 10px; }"
					+ "</style></head>"
					+ "<body>"
					+ "<div id=math></div></body>";
			w.loadDataWithBaseURL("http://bar/", html, mimeType, encoding, "");

			final int index = i;
			w.setWebViewClient(new WebViewClient() {
				@Override
				public void onPageFinished(WebView view, String url) {
					super.onPageFinished(view, url);
					loadUrlCompat(w, questions[index]);
				}
			});

			//if no saved level -> start at default level
			//TODO: preference lookup

			//add to main view
			if (i % WV_PER_ROW == 0) {
				r = new TableRow(this);
				answers.addView(r);
			}
			r.addView(w);
		}

		//bottom bar progress
		TextView t = makeLevelText();
		t.setText("1");
	}


	private void loadUrlCompat(WebView w, String content) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
			w.loadUrl("javascript:document.getElementById('math').innerHTML='\\\\["
					+ doubleEscapeTeX(content) //tex content
					+ "\\\\]';");
		} else {
			w.evaluateJavascript("javascript:document.getElementById('math').innerHTML='\\\\["
					+ doubleEscapeTeX(content) //tex content
					+ "\\\\]';", null);
		}

		w.loadUrl("javascript:MathJax.Hub.Queue(['Typeset',MathJax.Hub]);");
	}

	private String doubleEscapeTeX(String s) {
		String t="";
		for (int i=0; i < s.length(); i++) {
			if (s.charAt(i) == '\'') t += '\\';
			if (s.charAt(i) != '\n') t += s.charAt(i);
			if (s.charAt(i) == '\\') t += "\\";
		}
		return t;
	}

	private TextView makeLevelText() {
		LinearLayout bottomProgress = (LinearLayout) findViewById(R.id.game_progress);
		TextView tv = new TextView(this);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		int margin = (int) getResources().getDimension(R.dimen.default_height);
		params.setMargins(margin, margin, margin, margin);
		tv.setBackgroundResource(R.mipmap.answer_bg);
		tv.setGravity(Gravity.CENTER);
		tv.setPadding(0, 0, 0, getPixels(10));
		tv.setLayoutParams(params);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
			tv.setTextAppearance(this, R.style.AppTheme_TextViewProgress);
		else
			tv.setTextAppearance(R.style.AppTheme_TextViewProgress);

		bottomProgress.addView(tv);
		return tv;
	}

	private int getPixels(int dp) {
		float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
		return ((int) pixels);
	}
}
